# Pong3D_Unity

Build the game with version - Unity 5.6.4f1

check the develop branch

This is a pong game made in unity. It uses 3 different types of AI algorithms
1. Seek
2. Rule based decision making
3. State machines

Player Instructions
Run Pong3D.exe

Note: Pong3D_Data directory will be needed to present in the same directory as Pong3D.exe to run the game. It is provided.

To play:
	Press W and S to Move in menus. Press Spacebar to select
    Press Arrow Keys to move and turn the racket
    Press X to cycle between differen AI
	Press C to toggle between cameras